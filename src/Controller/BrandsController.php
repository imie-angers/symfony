<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use App\Entity\Brand;
use App\Form\BrandType;

/**
 * @Route("/brands")
 */
class BrandsController extends AbstractController
{
    /**
     * @Route("", name="brands")
     * @Template("brands/index.html.twig")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();

        $brands = $em
            ->getRepository(Brand::class)
            ->findAll()
        ;

        return [
            "brands" => $brands,
        ];
    }

    /**
     * @Route("/{id}/remove", name="brands_remove")
     * @ParamConverter("brand", class=Brand::class)
     */
    public function remove(Brand $brand)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($brand);
        $em->flush();

        return $this->redirectToRoute("brands");
    }

    /** 
     * @Route("/create", name="brands_create")
     * @Template("brands/create.html.twig")
     */
    public function create(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $brand = new Brand();
        $form = $this->createForm(BrandType::class, $brand);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($brand);
            $em->flush();

            return $this->redirectToRoute("brands");
        }

        return [
            "form" => $form->createView(),
        ];
    }

    /** 
     * @Route("/{id}/view", name="brands_view")
     * @Template("brands/view.html.twig")
     * @ParamConverter("brand", class=Brand::class)
     */
    public function view(Request $request, Brand $brand)
    {
        return [
            "brand" => $brand,
        ];
    }

    /** 
     * @Route("/{id}/edit", name="brands_edit")
     * @Template("brands/edit.html.twig")
     * @ParamConverter("brand", class=Brand::class)
     */
    public function edit(Request $request, Brand $brand)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(BrandType::class, $brand);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($brand);
            $em->flush();

            return $this->redirectToRoute("brands");
        }

        return [
            "form" => $form->createView(),
        ];
    }

}

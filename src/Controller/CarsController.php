<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use App\Entity\Car;
use App\Entity\Brand;
use App\Form\CarType;

class CarsController extends AbstractController
{
    /**
     * @Route("/", name="cars")
     * @Template("cars/index.html.twig")
     */
    public function index(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $keywords = $request->get("keywords");
        if(!is_null($keywords)) {
            $cars = $em
                ->createQuery('
                    SELECT c
                    FROM App\Entity\Car c
                    LEFT JOIN c.brand b
                    WHERE 
                        c.name LIKE :keywords OR
                        c.version LIKE :keywords OR
                        b.name LIKE :keywords
                ')
                ->setParameter('keywords', "%".$keywords."%")
                ->execute()
            ;
        } else {
            $cars = $em
                ->getRepository(Car::class)
                ->findAll()
            ; 
        }

        return [
            "cars" => $cars,
        ];
    }

    /**
     * @Route("/cars/{id}/remove", name="cars_remove")
     * @ParamConverter("car", class=Car::class)
     */
    public function remove(Car $car)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($car);
        $em->flush();

        return $this->redirectToRoute("cars");
    }

    /** 
     * @Route("/cars/create", name="cars_create")
     * @Template("cars/create.html.twig")
     */
    public function create(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $car = new Car();
        $form = $this->createForm(CarType::class, $car);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($car);
            $em->flush();

            return $this->redirectToRoute("cars");
        }

        return [
            "form" => $form->createView(),
        ];
    }

    /** 
     * @Route("/cars/{id}/view", name="cars_view")
     * @Template("cars/view.html.twig")
     * @ParamConverter("car", class=Car::class)
     */
    public function view(Request $request, Car $car)
    {
        return [
            "car" => $car,
        ];
    }

    /** 
     * @Route("/cars/{id}/edit", name="cars_edit")
     * @Template("cars/edit.html.twig")
     * @ParamConverter("car", class=Car::class)
     */
    public function edit(Request $request, Car $car)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(CarType::class, $car);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($car);
            $em->flush();

            return $this->redirectToRoute("cars");
        }

        return [
            "form" => $form->createView(),
        ];
    }

}
